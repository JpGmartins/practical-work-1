package AgenteReactivo;

public class Mundo {

  private Posicao [][] matriz;
  private Agente agente;

  // construtor
  public Mundo(){
    // O mundo onde se desloca o agente � uma matriz 10 por 10
	matriz = new Posicao[10][10];

    for (int i = 0; i < matriz.length; i++)
      for (int j = 0; j < matriz[0].length; j++) {
        matriz[i][j] = new Posicao(i, j);
      }

    // Defini�ao dos obst�culos existentes no mundo e que o agente dever� evitar
    matriz[2][2].setObstaculo(new Obstaculo());
    matriz[2][3].setObstaculo(new Obstaculo());
    matriz[4][2].setObstaculo(new Obstaculo());
    matriz[3][5].setObstaculo(new Obstaculo());
    matriz[2][4].setObstaculo(new Obstaculo());
    matriz[6][6].setObstaculo(new Obstaculo());
    matriz[4][6].setObstaculo(new Obstaculo());
    
    // cria��o do agente 
    agente = new Agente();
    // posi��o inicial do agente no canto superior esquerdo
    agente.setPosicao(getMatriz(0, 0));
  }

  public static void main(String[] str){
    // inicializa��o do mundo
    Mundo mundo = new Mundo();

    mundo.evoluir();
  }

  public void evoluir(){
     agente.accao(this);
  }


  public void setAgente(Agente agente) {
    this.agente = agente;
  }

  public Agente getAgente() {
    return agente;
  }

  public int getDimX(){
    return matriz.length;
  }
  public int getDimY(){
    return matriz[0].length;
  }

  public Posicao getMatriz(int x, int y) {
    return matriz[x][y];
  }

  public Posicao getPosicaoNorte(Posicao quadricula) {
    if (quadricula.getY() > 0)
      return matriz[quadricula.getX()][quadricula.getY() - 1];
    return null;
  }

  public Posicao getPosicaoSul(Posicao quadricula) {
    if (quadricula.getY() < matriz.length - 1)
      return matriz[quadricula.getX()][quadricula.getY() + 1];
    return null;
  }

  public Posicao getPosicaoOeste(Posicao quadricula) {
    if (quadricula.getX() > 0)
      return matriz[quadricula.getX() - 1][quadricula.getY()];
    return null;
  }

  public Posicao getPosicaoEste(Posicao quadricula) {
    if (quadricula.getX() < matriz.length - 1)
      return matriz[quadricula.getX() + 1][quadricula.getY()];
    return null;
  }

  public String toString(){
    StringBuffer s = new StringBuffer();
    for (int i = 0; i < matriz.length; i++) {
      for (int j = 0; j < matriz[0].length; j++) {
        if(matriz[j][i].getObstaculo() != null)
          s.append(" O ");
        else
        if(matriz[j][i].getAgente() != null)
          s.append(" A ");
        else
          s.append(" - ");
      }
      s.append('\n');
    }
    return s.toString();
  }
}
