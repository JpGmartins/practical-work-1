package AgenteReactivo;

// Classe que permite gerar um n�mero aleat�rio que ser� associado a uma das 4 direc��es possiveis de movimento
import java.util.Random;

public class Agente {

  private Posicao posicao;
  
  public Agente(){
  }

  public void accao(Mundo mundo){
    System.out.println("In�cio da execu��o do programa -->");
     
    // escreve no ecr� a matriz que representa o mundo
    System.out.println(mundo);

    // O programa � executado em 20 iterac��es. O agente constr�i uma percep��o do que o rodeia e ter� que decidir baseado nisso
    for(int i = 0; i < 20; i++){
      executar(decidir(construirPercepcao(mundo)), mundo);
    }
  }

  public Posicao getPosicao() {
    return posicao;
  }

  public void setPosicao(Posicao posicao) {
    this.posicao = posicao;
    this.posicao.setAgente(this);
  }

  // devolve as posi��es a N, S, E e O 
  private Percepcao construirPercepcao(Mundo mundo){
    return new Percepcao(
          mundo.getPosicaoNorte(posicao),
          mundo.getPosicaoSul(posicao),
          mundo.getPosicaoEste(posicao),
          mundo.getPosicaoOeste(posicao));
  }

 
  // Alterar
  private Movimento decidir(Percepcao percepcao){
	  Random aleatorios = new Random();
	  boolean colisaoObstaculo = false;
	  int direccao;
      // ...
      
      // ciclo que dever� executar enquanto o movimento do agente o fizer colidir com um obst�culo
     
      do{
          if(posicao.getX()<8&&percepcao.getE().getObstaculo()==null){
              return Movimento.este;
          }
          else if (posicao.getX() > 8 && percepcao.getO().getObstaculo() == null){
              return Movimento.oeste;
          }
          else if (posicao.getY() > 8 && percepcao.getN().getObstaculo() == null){
              return Movimento.norte;
          }
          else if (posicao.getY() < 8 && percepcao.getS().getObstaculo() == null){
              return Movimento.sul;
          }
          else colisaoObstaculo = true;
      }while(colisaoObstaculo);          

    return null;
   
  }

  private void executar(Movimento accao, Mundo mundo){

    if(accao == Movimento.norte && posicao.getY() != 0){
      this.posicao.setAgente(null);
      this.setPosicao(mundo.getPosicaoNorte(posicao));
      }
    else if(accao == Movimento.sul && posicao.getY() != mundo.getDimY()){
          this.posicao.setAgente(null);
          this.setPosicao(mundo.getPosicaoSul(posicao));
        }
    else if(accao == Movimento.oeste && posicao.getX() != 0){
          this.posicao.setAgente(null);
          this.setPosicao(mundo.getPosicaoOeste(posicao));
        }
    else if(accao == Movimento.este && posicao.getX() != mundo.getDimX()){
          this.posicao.setAgente(null);
          this.setPosicao(mundo.getPosicaoEste(posicao));
        }
    System.out.println(mundo);
  }
}