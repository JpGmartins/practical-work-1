package AgenteReactivo;

public class Posicao {
  private int x, y;
  private Agente agente;
  private Obstaculo obstaculo;

  public Posicao(int x, int y){
    this.x = x;
    this.y = y;
  }

  public Agente getAgente() {
    return agente;
  }

  public void setAgente(Agente agente) {
    this.agente = agente;
  }

  public Obstaculo getObstaculo() {
    return obstaculo;
  }

  public void setObstaculo(Obstaculo obstaculo) {
    this.obstaculo = obstaculo;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }
}