package AgenteReactivo;

public class Movimento {
  private String descricao;

  public static final Movimento norte = new Movimento("Norte");
  public static final Movimento sul = new Movimento("Sul");
  public static final Movimento este = new Movimento("Este");
  public static final Movimento oeste = new Movimento("Oeste");

  public Movimento(String descricao){
    this.descricao = descricao;
  }

  public String getDescricao() {
    return descricao;
  }
}