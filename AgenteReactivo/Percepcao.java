package AgenteReactivo;

public class Percepcao {
  private Posicao N, S, E, O;

  public Percepcao(Posicao N, Posicao S, Posicao E,Posicao O){
    this.N = N;
    this.S = S;
    this.E = E;
    this.O = O;
  }

  public Posicao getE() {
    return E;
  }

  public Posicao getN() {
    return N;
  }

  public Posicao getO() {
    return O;
  }

  public Posicao getS() {
    return S;
  }
}